////
////  MarvelAppTests.swift
////  MarvelAppTests
////
////  Created by c94283a on 19/11/21.
////
//
//import XCTest
//@testable import MarvelApp
//
//class MainViewControllerTests: XCTestCase {
//
//    private var sut: ViewModel!
//    private var api: ApiRequest!
//
//    override func setUp() {
//
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//        sut = (storyboard.instantiateViewController(withIdentifier: "marvelVc") as! ViewModel)
//
//    }
//
//    func testCalculatingTotalHerosInScreen_AfterApiIsRequested()  {
//        let expect = expectation(description: "Expect success")
//
//        _ = self.sut.view
//
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0){
//
//            XCTAssertEqual(self.sut.arrayList.count, 20)
//            expect.fulfill()
//        }
//        waitForExpectations(timeout: 10.0)
//    }
//
//
//
//
//
//    func testHerosInAllCollectionSumTotalHeros_AfterApiIsRequested()  {
//
//        let expect = expectation(description: "Expect success")
//
//        _ = self.sut.view
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0){
//
//            XCTAssertEqual(self.sut.arrayList.count, self.sut.arrayCarousel.count + self.sut.arrayList.count)
//            expect.fulfill()
//        }
//        waitForExpectations(timeout: 10.0)
//    }
//
//    func testHerosInAllCollectionSumTotalHeros_WhenViewControllerStarts()  {
//
//        XCTAssertEqual(sut.arrayList.count, sut.arrayCarousel.count + sut.arrayList.count)
//    }
//
//
//
//
//}
//
