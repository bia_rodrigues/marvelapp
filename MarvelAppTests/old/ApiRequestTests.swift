//
//  ApiRequestTests.swift
//  MarvelAppTests
//
//  Created by c94283a on 03/12/21.
//
//
//import XCTest
//@testable import MarvelApp
//
//class ApiRequestTests: XCTestCase {
//    
//    private var sut: ApiRequest!
//    private var arrayTest : [Heroi] = []
//    override func setUp() {
//        sut = ApiRequest()
//    }
//    
//    
//    func testIfFirstReturnFromApiStartsWhereItShould_beforeFirstCall()  {
//        XCTAssertEqual(self.sut.nextFirstReturnFromApi, 0)
//    }
//    
//    func testIfSomeHeroIsReturnedFromApi_AfterFirstCall()  {
//        let expect = expectation(description: "Expect success")
//        
//        sut.makeRequest { result in
//            switch result {
//            case .success(let result):
//                XCTAssertGreaterThan(result.data.listaDeHerois.count, 0)
//            case .failure:
//                XCTFail("Fail")
//            }
//            expect.fulfill()
//        }
//        waitForExpectations(timeout: 4.0)
//    }
//    
//}

