//
//  MockDataBaseController.swift
//  MarvelAppTests
//
//  Created by c94283a on 25/01/22.
//

import Foundation
@testable import MarvelApp
import CoreData

var apiCallls:Int = 0

class MockDataBaseController: DataBaseControllerProtocol{
    
    var herosSaved:Int = 0
    var listaDeHerois:[Heroi] = []
    
    func apiCacheSaver(_ listaDeHerois: [Heroi]) {
        let herosAdded = listaDeHerois.count
        self.listaDeHerois = listaDeHerois
        self.herosSaved+=herosAdded
    }
    
    func fetchData() -> [HeroInCache] {
        return[]
    }
    
    func saveContext() {
    }
    
}
