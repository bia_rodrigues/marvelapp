//
//  MockAnalytics.swift
//  MarvelAppTests
//
//  Created by c94283a on 31/01/22.
//

import Foundation
@testable import MarvelApp

struct MockAnalyticsManager: AnalyticsProtocol{
    func screenAnalytics(screenName: String, screenClass: String) {
        
    }
    
    func setUserPropertyForAnalytics(property: String, forName: String) {
        
    }
    
    func setLogEventForAnalytics(key: String, heroName: String) {
        
    }
    
    func setUserIDForAnalytics() {
        
    }
  
}
