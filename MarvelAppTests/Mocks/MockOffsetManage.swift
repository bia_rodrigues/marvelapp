//
//  MockOffsetManage.swift
//  MarvelAppTests
//
//  Created by c94283a on 30/01/22.
//

@testable import MarvelApp
import Foundation

struct MockOffsetManage: ProtocolOffsetManage{
    
    var testOffset:Int = 0
    
    func apiOffsetSaver(offset: Int16) {
    }
    
    mutating func fetchOffset() -> Int16 {
        return Int16(testOffset)
    }
    
    
}
