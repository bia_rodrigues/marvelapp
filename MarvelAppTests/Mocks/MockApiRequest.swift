//
//  MockApiRequest.swift
//  MarvelAppTests
//
//  Created by c94283a on 19/01/22.
//

@testable import MarvelApp
import Foundation
import CoreData

var offsetManage = OffsetManage()
var isRequestingApi : Bool = false

class MockApiRequestSuccess: RequestProtocol{
    var urlProvider: URLProviderProtocol
    
    init(urlProvider: URLProviderProtocol){
        self.urlProvider = urlProvider
    }
    
    var isRequestingApi: Bool = false
    var url: String = ""
    var shouldIncreaseOffset: Bool = false
    var apiCallls:Int = 0
    
    func makeRequest(completion: @escaping (Result<Welcome, Error>) -> ()) {
        isRequestingApi = true
        apiCallls += 1
        
        guard let responseUrl = Bundle.main.url(forResource: "response", withExtension: "json") else {return}
        guard let jsonData = try? Data(contentsOf: responseUrl) else {return}
        isRequestingApi = false
        do{
            let data = try JSONDecoder().decode(Welcome.self, from: jsonData)
            
            completion(.success(data))
        }catch{
            print(error.localizedDescription)
        }
    }
}
