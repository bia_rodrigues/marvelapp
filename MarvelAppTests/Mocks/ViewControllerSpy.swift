//
//  MockView.swift
//  MarvelAppTests
//
//  Created by c94283a on 24/01/22.
//


import Foundation
@testable import MarvelApp

class ViewControllerSpy: ViewProtocol{
    func showAnimation() {
    }
    
    func hideAnimation() {
    }
    
    func reloadContentUp() {
    }
    
    func reloadContentDown() {
    }
    
    func showApiErrorAlert(alertTitle: String, alertMenssage: String) {
    }
    
 
}
