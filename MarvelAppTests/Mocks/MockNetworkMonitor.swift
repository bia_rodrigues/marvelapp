//
//  MockNetworkMonitor.swift
//  MarvelAppTests
//
//  Created by c94283a on 24/01/22.
//

import Foundation
@testable import MarvelApp

class MockNetworkMonitor: NetworkMonitorProtocol{
    var isReachable: Bool = true
    
    func startMonitoring() {
    }
    
    func stopMonitoring() {
    }
}
