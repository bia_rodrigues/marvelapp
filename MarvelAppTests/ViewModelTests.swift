//
//  ViewModelTests.swift
//  MarvelAppTests
//
//  Created by c94283a on 20/01/22.
//

import XCTest
@testable import MarvelApp

class ViewModelTests: XCTestCase{
    
    var viewModel: ViewModel?
    var view = ViewControllerSpy()
    var offSetManage = MockOffsetManage()
    var apiRequest = MockApiRequestSuccess(urlProvider: URLProvider(offsetManage: MockOffsetManage()))
    var dataBaseController = MockDataBaseController()
    
    override func tearDownWithError() throws {
        viewModel = nil
    }
    
    override func setUp() {
        super.setUp()
        viewModel = ViewModel(view: view, apiRequest: apiRequest, monitor: MockNetworkMonitor(), dataBaseController: dataBaseController, urlProvider: URLProvider(offsetManage: offSetManage), analytics: MockAnalyticsManager())
    }
    
    func testReturnFromFirstCallToAPI_whenItsCalled_ShoulMakeRequestCallToAPIJustOnce(){
        apiRequest.isRequestingApi = false
        viewModel?.firstCallToApi()
        XCTAssertEqual(apiRequest.apiCallls, 1)
    }
    
    func testReturnFromFirstCallToAPI_whenItsCalled_ShoulAddObjectsToCoredata(){
        apiRequest.isRequestingApi = false
        offSetManage.testOffset = 0
        viewModel?.firstCallToApi()

        let numberOfHerosBringFromApi: Int = dataBaseController.herosSaved
            XCTAssertGreaterThan(numberOfHerosBringFromApi, 0)

    }
    
    func testReturnFromFirstCallToAPI_whenItsCalled_ShoulAddTwentyObjectsToCoredata(){
        apiRequest.isRequestingApi = false
        offSetManage.testOffset = 0
        viewModel?.firstCallToApi()
        let numberOfHerosInCache: Int = dataBaseController.listaDeHerois.count
            XCTAssertEqual(numberOfHerosInCache, 20)
    }
    
    func testIncrementalCallToApi_whenItsCalled_ShouldCallAPIJustOnce(){
        let callsBefore = apiRequest.apiCallls
        offSetManage.testOffset = 20
        apiRequest.isRequestingApi = false
        viewModel?.incrementalCallToApi()
        let callsAfter = apiRequest.apiCallls
        XCTAssertEqual(callsAfter - callsBefore, 1)
    }
    
    
    func testApiCallsEffectiveness_whenItsFirstCallToAPIisCalled_ShoulShowAllDataCalled(){
        apiRequest.isRequestingApi = false
        offSetManage.testOffset = 0
        viewModel?.firstCallToApi()
        let data = dataBaseController.listaDeHerois.count
        let quantityOfDataIfAllCallsWereSuccessful = apiRequest.apiCallls * 20
            XCTAssertEqual(data, quantityOfDataIfAllCallsWereSuccessful)
    }
    
    func testApiCallsEffectiveness_whenItsIncrementalCallToAPIisCalled_ShoulShowAllDataCalled(){
        apiRequest.isRequestingApi = false
        offSetManage.testOffset = 20
        viewModel?.incrementalCallToApi()
        let data = dataBaseController.listaDeHerois.count
        let quantityOfDataIfAllCallsWereSuccessful = apiRequest.apiCallls * 20
            XCTAssertEqual(data, quantityOfDataIfAllCallsWereSuccessful)
    }
    
    
    func testBackFromSearch_whenSearchBarGetClean_ShouldShowAllDataShowedBeforeSearchAgain(){
                var dataOnTop = viewModel?.arrayCarousel.count ?? 0
        var dataDown = viewModel?.arrayList.count ?? 0
        let dataOnScreenBeforeSearchAndClean =  dataOnTop + dataDown
        
        viewModel?.searchCallToApi(searchHeroStartingWith: "a")
        viewModel?.searchCallToApi(searchHeroStartingWith: "")
        dataOnTop = viewModel?.arrayCarousel.count ?? 0
        dataDown = viewModel?.arrayList.count ?? 0
        let dataOnScreenAfterearchAndClean =  dataOnTop + dataDown
            XCTAssertEqual(dataOnScreenBeforeSearchAndClean, dataOnScreenAfterearchAndClean)

    }
    
}
