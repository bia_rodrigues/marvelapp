# MarvelApp


## Estado atual do App:

A tela inicial do app apresenta duas collection views, uma com orientação de scroll horizontal e outra vertical com dados, imagem e nome, de personagens vindos da API da Marvel.
A collection view com scroll vertical requisita mais personagens em uma nova chamada de api cada vez que o usuário vizualiza o último personagem já exibido na tela.
Ao selecionar a célula de qualquer dos personagens na primeira tela se tem acesso a segunda tela do app. A segunda tela do app contém a imagem da célula selecionada ampliada, o nome do personagem e uma lista dos nomes dos quadrinhos nos quais esse personagem figurou.
O app apresenta testes unitários síncronos, testes que evidenciam se a API se comporta conforme o esperado quando é requisitada.
    

## Etapas de construção do App:

[x]  1. Setup Inicial - MVC + Navegação + Interfaces;<br />
[x]  2. Integração API;<br />
[x]  3. Testes unitários;<br />
[x]  4. Refactoring - Uso Persistência interna;<br />
[x]  5. Filtro e pesquisa;<br />
[x]  6. Refactoring - Arquitetura MVVM;<br />
[x]  7. Acessibilidade;<br />
[x]  8. Tagueamento;


## Dependencies:

- Kingfisher;<br />
- Lottie 3.2.3
- Firebase 8.11.0
