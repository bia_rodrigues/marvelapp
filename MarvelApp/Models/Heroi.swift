
import Foundation

// MARK: - Welcome
struct Welcome: Codable {
    let code: Int
    let status: String
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let offset, limit, total, count: Int
    let listaDeHerois: [Heroi]
    
    enum CodingKeys: String, CodingKey {
        case offset, limit, total, count
        case listaDeHerois = "results"
    }
}

// MARK: - Result
public struct Heroi: Codable {
    let id: Int?
    let name: String?
    let thumbnail: Thumbnail?
    let comics: Comics?
    
}
