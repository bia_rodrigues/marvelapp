//
//  Comics.swift
//  MarvelApp
//
//  Created by c94283a on 15/12/21.
//

import Foundation

// MARK: - Comics
struct Comics: Codable {
    let items: [ComicsItem]?
}

// MARK: - ComicsItem
struct ComicsItem: Codable {
    let name: String?
}
