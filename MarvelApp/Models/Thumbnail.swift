//
//  Thumbnail.swift
//  MarvelApp
//
//  Created by c94283a on 15/12/21.
//

import Foundation

// MARK: - Thumbnail
struct Thumbnail: Codable {
    let path: String?
    let thumbnailExtension: ImageExtension?

    enum CodingKeys: String, CodingKey {
        case path
        case thumbnailExtension = "extension"
    }
}

enum ImageExtension: String, Codable {
    case gif = "gif"
    case jpg = "jpg"
}
