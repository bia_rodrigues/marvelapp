//
//  DetailViewController.swift
//  MarvelApp
//
//  Created by c94283a on 22/11/21.
//

import UIKit

class DetailViewController: UIViewController{
     
     lazy var detailViewModel: DetailViewModelProtocol = {
          let detailViewModel = DetailViewModel(analytics: FirebaseAnalytics())
          
          return detailViewModel
     }()
     
     var heroSelected: HeroInCache?
     var arrayOfStrings: [String] = ["None"]
     var hasAddGradient: Bool = false
     
     private let scrollView: UIScrollView = {
          let scrollView = UIScrollView()
          scrollView.translatesAutoresizingMaskIntoConstraints = false
          return scrollView
     }()
     
     private var backgroundView: UIView = {
          let backgroundView = UIView()
          backgroundView.translatesAutoresizingMaskIntoConstraints = false
          backgroundView.backgroundColor = UIColor(named: "MarvelBlack")
          return backgroundView
     }()
     
     private var stackView: UIStackView = {
          let stackView = UIStackView()
          stackView.translatesAutoresizingMaskIntoConstraints = false
          stackView.axis = .vertical
          stackView.distribution = .fill
          return stackView
     }()
     
     private var viewOfImageInDetailPage: UIView = {
          let view = UIView()
          view.translatesAutoresizingMaskIntoConstraints = false
          return view
     }()
     
     private var imageInDetailPage: DetailGradient = {
          let imagem = DetailGradient()
          imagem.translatesAutoresizingMaskIntoConstraints = false
          return imagem
     }()
     
     private var labelOfTitleName: UILabel = {
          let label = UILabel()
          label.translatesAutoresizingMaskIntoConstraints = false
          label.textColor = .white
          label.font = UIFont(name: "Apple SD Gothic Neo Bold", size: 30)
          label.numberOfLines = 0
          return label
     }()
     
     private var viewOfSubtitleBiography: UIView = {
          let view = UIView()
          view.translatesAutoresizingMaskIntoConstraints = false
          return view
     }()
     
     private var labelOfsubtitleBiography: UILabel = {
          let label = UILabel()
          label.translatesAutoresizingMaskIntoConstraints = false
          label.text = "Comics this Character appears in"
          label.font = UIFont(name: "Apple SD Gothic Neo Bold", size: 16)
          label.textColor = .white
          return label
     }()
     
     private var viewOfBiography: UIView = {
          let view = UIView()
          view.translatesAutoresizingMaskIntoConstraints = false
          return view
     }()
     
     private var labelOfBiography: UILabel = {
          let label = UILabel()
          label.translatesAutoresizingMaskIntoConstraints = false
          label.text = ""
          label.textColor = .white
          label.numberOfLines = 0
          label.font = UIFont(name: "Apple SD Gothic Neo", size: 16)
          return label
     }()
     
     override func viewDidLoad(){
          super.viewDidLoad()
          setupView()
          setupData()
          dataForAnalitycs()
     }
     
     func setupView(){
          self.navigationItem.titleView = UIImageView(image: UIImage(named: "Logo"))
          self.navigationController?.navigationBar.backgroundColor = UIColor(red: 32.0/255.0, green: 32.0/255.0, blue: 32.0/255.0, alpha: 1.0)
          
          view.addSubview(backgroundView)
          NSLayoutConstraint.activate([
               backgroundView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
               backgroundView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
               backgroundView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
               backgroundView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
          ])
          
          // MARK: - add scrol on safe area
          view.addSubview(scrollView)
          NSLayoutConstraint.activate([
               scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
               scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
               scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
               scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
          ])
          
          // MARK: - add stack on scroll
          scrollView.addSubview(stackView)
          NSLayoutConstraint.activate([
               stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
               stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
               stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
               stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
               stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
          ])
          
          // MARK: - add view of image and title label  in stack
          stackView.addArrangedSubview(viewOfImageInDetailPage)
          viewOfImageInDetailPage.addSubview(imageInDetailPage)
          NSLayoutConstraint.activate([
               imageInDetailPage.topAnchor.constraint(equalTo: viewOfImageInDetailPage.topAnchor),
               imageInDetailPage.heightAnchor.constraint(equalTo: imageInDetailPage.widthAnchor),
               imageInDetailPage.widthAnchor.constraint(equalTo: view.widthAnchor)
          ])
          
          viewOfImageInDetailPage.addSubview(labelOfTitleName)
          labelOfTitleName.textColor = .white
          NSLayoutConstraint.activate([
               labelOfTitleName.topAnchor.constraint(equalTo: imageInDetailPage.bottomAnchor),
               labelOfTitleName.leadingAnchor.constraint(equalTo: viewOfImageInDetailPage.leadingAnchor, constant: 28),
               labelOfTitleName.trailingAnchor.constraint(equalTo: viewOfImageInDetailPage.trailingAnchor, constant: -28),
               labelOfTitleName.bottomAnchor.constraint(equalTo: viewOfImageInDetailPage.bottomAnchor)
          ])
          
          // MARK: - add Subtitle Biography in stack under  image
          stackView.addArrangedSubview(viewOfSubtitleBiography)
          
          viewOfSubtitleBiography.addSubview(labelOfsubtitleBiography)
          NSLayoutConstraint.activate([
               labelOfsubtitleBiography.topAnchor.constraint(equalTo: viewOfSubtitleBiography.topAnchor, constant: 28),
               labelOfsubtitleBiography.leadingAnchor.constraint(equalTo: viewOfSubtitleBiography.leadingAnchor, constant: 28),
               labelOfsubtitleBiography.trailingAnchor.constraint(equalTo: viewOfSubtitleBiography.trailingAnchor, constant: -28),
               labelOfsubtitleBiography.bottomAnchor.constraint(equalTo: viewOfSubtitleBiography.bottomAnchor, constant: -8)
          ])
          
          // MARK: - add Biography in stack under  Subtitle Biography
          stackView.addArrangedSubview(viewOfBiography)
          
          viewOfBiography.addSubview(labelOfBiography)
          NSLayoutConstraint.activate([
               labelOfBiography.topAnchor.constraint(equalTo: viewOfBiography.topAnchor, constant: 0),
               labelOfBiography.leadingAnchor.constraint(equalTo: viewOfBiography.leadingAnchor, constant: 28),
               labelOfBiography.trailingAnchor.constraint(equalTo: viewOfBiography.trailingAnchor, constant: -28),
               labelOfBiography.bottomAnchor.constraint(equalTo: viewOfBiography.bottomAnchor, constant: -20)
          ])
     }
     
     func setupData(){
          guard let theHeroSelected = heroSelected else {return}
          
          labelOfTitleName.text = theHeroSelected.name
          var stringURL = theHeroSelected.image ?? ""
          if UIAccessibility.isVoiceOverRunning{
               stringURL = ""
          }
          guard let url: URL = URL(string: stringURL) else {return}
          imageInDetailPage.kf.setImage(with: url, placeholder: UIImage(named: ""), options: [.transition(.fade(0.7))], progressBlock: nil)
          imageInDetailPage.layer.cornerRadius = 14.0
          
          labelOfBiography.text! = theHeroSelected.comics ?? ""
     }
     
     func dataForAnalitycs(){
          guard let name: String = heroSelected?.name else {return}
          let nameAsParameterForAnalitycs: String = name.forSorting
          detailViewModel.analytics.setUserIDForAnalytics()
          detailViewModel.analytics.setUserPropertyForAnalytics(property: "food", forName: "apple")
          detailViewModel.analytics.screenAnalytics(screenName: "DetailScreen", screenClass: "DetailViewController" )
          detailViewModel.analytics.setLogEventForAnalytics(key: "hero_name", heroName: nameAsParameterForAnalitycs)
     }
}


extension String{
     var forSorting: String{
          let simple = folding(options: [.diacriticInsensitive, .widthInsensitive, .caseInsensitive], locale: nil)
          let nonAlphaNumeric = CharacterSet.alphanumerics.inverted
          let jointed = simple.components(separatedBy: nonAlphaNumeric).joined(separator: "_")
          return jointed.components(separatedBy: "__").joined(separator: "_")
     }
}

