//
//  DetailViewModel.swift
//  MarvelApp
//
//  Created by c94283a on 02/02/22.
//

import Foundation

protocol DetailViewModelProtocol {
    var analytics: AnalyticsProtocol{get set}
}

class DetailViewModel: DetailViewModelProtocol{
    
    var analytics: AnalyticsProtocol
    
    init( analytics: AnalyticsProtocol) {
        self.analytics = analytics
    }
}
