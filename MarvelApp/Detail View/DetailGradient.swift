//
//  DetailGradient.swift
//  MarvelApp
//
//  Created by c94283a on 09/12/21.
//

import Foundation
import UIKit

class DetailGradient : UIImageView {
    
    override func layoutSubviews() {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor(named: "MarvelBlack")!.withAlphaComponent(1.0).cgColor, UIColor(named: "MarvelBlack")!.withAlphaComponent(0.0).cgColor]
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.9)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.7)
        self.layer.insertSublayer(gradientLayer, at: 0)
        
    }
}
