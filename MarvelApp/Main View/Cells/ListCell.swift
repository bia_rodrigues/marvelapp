//
//  ListCell.swift
//  MarvelApp
//
//  Created by c94283a on 19/11/21.
//

import UIKit

class ListCell: UICollectionViewCell {
    
    @IBOutlet weak var listImage: UIImageView!
    @IBOutlet weak var listLabel: UILabel!
    
    var hasAddGradient : Bool = false
    
    override func layoutSubviews() {
        if hasAddGradient == false {
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [UIColor.black.withAlphaComponent(1.0).cgColor, UIColor.black.withAlphaComponent(0.0).cgColor]
            gradientLayer.frame = listImage.frame
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.9)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.7)
            gradientLayer.cornerRadius = 14.0
            listImage.layer.insertSublayer(gradientLayer, at: 0)
            hasAddGradient = true
            
        }
    }
    
    
}

