//
//  CarouselCell.swift
//  MarvelApp
//
//  Created by c94283a on 19/11/21.
//

import UIKit
import Kingfisher

class CarouselCell: UICollectionViewCell {
    
    @IBOutlet weak var carouselImage: UIImageView!
    @IBOutlet weak var carouselLabel: UILabel!
    
    var hasAddGradient : Bool = false
    
    override func layoutSubviews() {
        if hasAddGradient == false {
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [UIColor.black.withAlphaComponent(1.0).cgColor, UIColor.black.withAlphaComponent(0.0).cgColor]
            gradientLayer.frame = carouselImage.frame
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.9)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.7)
            gradientLayer.cornerRadius = 14.0
            carouselImage.layer.insertSublayer(gradientLayer, at: 0)
            hasAddGradient = true
        }
    }
}
