//
//  ViewModel.swift
//  MarvelApp
//
//  Created by c94283a on 13/01/22.
//

import Foundation
import CoreData

protocol ViewModelProtocol {
    
    var view: ViewProtocol { get set }
    var apiRequest: RequestProtocol { get set }
    //   var monitor: NetworkMonitorProtocol
    var dataBaseController: DataBaseControllerProtocol{ get }
    var analytics: AnalyticsProtocol{get set}
    
    var heroInCache: [HeroInCache] { get }
    var arrayCarousel: [HeroInCache] { get }
    var arrayList: [HeroInCache] { get set }
    var arrayListBackUp: [HeroInCache] { get set }
    var filteredData: [HeroInCache] { get set }
    var searchResultsFromApiByName: [HeroInCache] { get set }
    var returnFromApi: [Heroi] { get set }
    
    func loadContent()
    func firstCallToApi()
    func incrementalCallToApi()
//    func searchCallToApi(searchHeroStartingWith:String)
    func callToApi(callback: @escaping (Welcome) -> Void )

}

class ViewModel: ViewModelProtocol {
    
    var view: ViewProtocol
    var apiRequest: RequestProtocol
    var monitor: NetworkMonitorProtocol
    var dataBaseController: DataBaseControllerProtocol
    var urlProvider: URLProviderProtocol
    var analytics: AnalyticsProtocol
    
    var returnFromApi: [Heroi] = []
    var heroInCache: [HeroInCache] = [] //objetos no coredata apos método fethdata ser chamado
    var arrayCarousel: [HeroInCache] = [] //array horizontal
    var arrayList: [HeroInCache] = [] //array vertical
    var arrayListBackUp: [HeroInCache] = []
    var filteredData: [HeroInCache]=[]
    var searchResultsFromApiByName: [HeroInCache] = []
    
    //TODO: should monitor stay in vc or vm
    
    init(view: ViewProtocol, apiRequest: RequestProtocol, monitor: NetworkMonitorProtocol ,dataBaseController: DataBaseControllerProtocol, urlProvider: URLProviderProtocol, analytics: AnalyticsProtocol) {
        self.view = view
        self.apiRequest = apiRequest
        self.monitor = monitor
        self.dataBaseController = dataBaseController
        self.urlProvider = urlProvider
        self.analytics = analytics
    }
    
    
    lazy var context = DataBaseController.persistentContainer.viewContext
    
    
    func loadContent(){
        view.showAnimation()
        heroInCache = dataBaseController.fetchData()
        if heroInCache.isEmpty {
            firstCallToApi()
        } else {
            showDataInCarosel()
            showDataInList()
        }
    }
    
    // MARK: - Show data on Screen
    
    func showDataInCarosel() {
        self.arrayCarousel.append(contentsOf: self.heroInCache.prefix(5))
        view.reloadContentUp()
    }
    
    func showDataInList() {
        if self.heroInCache.count > 5{
            self.arrayList.append(contentsOf:self.heroInCache.suffix(from: 5))
            view.reloadContentDown()
        }
    }
    
    func showIncrementalDataInList(nextOffSet: Int) {
        let firstIncrementalDataPosition = nextOffSet - 20
        if firstIncrementalDataPosition > 0 {
        self.arrayList.append(contentsOf:self.heroInCache.suffix(from: firstIncrementalDataPosition))
        view.reloadContentUp()
        view.reloadContentDown()
        }
    }
}


