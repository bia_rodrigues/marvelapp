//
//  DataBaseController.swift
//  MarvelApp
//
//  Created by c94283a on 15/12/21.
//

import Foundation
import CoreData

protocol DataBaseControllerProtocol{
    func saveContext()
    func apiCacheSaver(_ listaDeHerois: [Heroi])
    func fetchData()-> [HeroInCache]
}

// MARK: - Core Data stack

class DataBaseController: DataBaseControllerProtocol{
    
    lazy var context = DataBaseController.persistentContainer.viewContext
    static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Data")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = DataBaseController.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func fetchData()-> [HeroInCache]{
        do{
            let dataFromCache = try DataBaseController.persistentContainer.viewContext.fetch(HeroInCache.fetchRequest())
            return dataFromCache
        }
        catch{
            print("Erro in fetchData")
            return[]
        }
    }
    
    func apiCacheSaver(_ listaDeHerois: [Heroi]) {
        
        let commitedHeros = self.fetchData()
        let commitedHerosIds: [Int] = commitedHeros.map{Int(bitPattern: $0.id)}
        
        for heroi in listaDeHerois {
            guard let safeId = heroi.id else {return}
            if !commitedHerosIds.contains(safeId){
                let newHeroInCache = HeroInCache(context: context)//
                
                newHeroInCache.name = heroi.name ?? ""
                
                newHeroInCache.id = String(safeId)
                
                if let safeThumbnail = heroi.thumbnail {
                    let path = safeThumbnail.path
                    let size = "standard_amazing"
                    let extensionURL = safeThumbnail.thumbnailExtension
                    guard let safeExtensionURL = extensionURL, let safePath = path else {return}
                    let imageAsString = "\(safePath)/\(size).\(safeExtensionURL)"
                    newHeroInCache.image = imageAsString
                }
                
                var comicsAsString = ""
                if let  safeItems = heroi.comics?.items{
                    for safeItem in safeItems  {
                        if let safeName = safeItem.name{
                        comicsAsString += safeName+" \n"
                        }
                    }
                }
                if comicsAsString.isEmpty {
                    comicsAsString = "This character did'nt appeared in any comics, according to Marvel's database"}
                
                newHeroInCache.comics = comicsAsString
            }
            saveContext()
        }
    }
    
    
    
}
