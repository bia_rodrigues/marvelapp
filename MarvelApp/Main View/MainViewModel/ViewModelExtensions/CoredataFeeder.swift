//
//  File.swift
//  MarvelApp
//
//  Created by c94283a on 11/01/22.
//

import Foundation

extension ViewModel{
    
    func firstCallToApi(){
        if apiRequest.isRequestingApi == false{
            apiRequest.shouldIncreaseOffset = true
            apiRequest.url = urlProvider.getURL()
            callToApi(callback: self.firstCallToApiReload)
            
        }
    }
    func firstCallToApiReload(data : Welcome){
        returnFromApi = data.data.listaDeHerois
        self.dataBaseController.apiCacheSaver(returnFromApi)
        self.heroInCache = self.dataBaseController.fetchData()
        self.showDataInCarosel()
        self.showDataInList()
    }
    
    func incrementalCallToApi(){
        if apiRequest.isRequestingApi == false{
            apiRequest.shouldIncreaseOffset = true
            apiRequest.url = urlProvider.getURL()
            callToApi(callback: self.incrementalCallToApiReload)
        }
    }
    func incrementalCallToApiReload(data : Welcome){
        self.dataBaseController.apiCacheSaver(data.data.listaDeHerois)
        self.heroInCache = self.dataBaseController.fetchData()
        self.showIncrementalDataInList(nextOffSet: self.heroInCache.count)
    }
    
    //outOfReach
    func searchCallToApi(searchHeroStartingWith:String){
        apiRequest.shouldIncreaseOffset = false
        //        apiRequest.isRequestingApi = true //
        apiRequest.url = urlProvider.getSearchURL(for: searchHeroStartingWith)
        callToApi(callback: self.searchCallToApiReload)
    }
    //outOfReach
    func searchCallToApiReload(data : Welcome){
        //that should't leave data inside context. If it does that, once apiCacheSaver get called, it would make order of data saved get confuse and may duplicate data
//        self.searchResultsFromApiByName = self.castingFromApiFormatToViewFormat(data.data.listaDeHerois)
        self.arrayList = self.searchResultsFromApiByName
        view.reloadContentDown()
    }
    
    
    func callToApi(callback: @escaping (Welcome) -> Void ){
        apiRequest.makeRequest(completion: {(posts) in
            switch posts {
            case .success(let dataResponseFromAPI):
                    callback(dataResponseFromAPI)
            case .failure(let failureResponseFromAPI):
                print(failureResponseFromAPI)
                self.view.showApiErrorAlert(alertTitle: "Ops...", alertMenssage: "Somethig went wrong beteween this app and the internet let's let it go for a while and try it again later, ok? ")
            }
        }
        )
    }
   
}
