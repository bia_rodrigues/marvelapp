//
//  ViewController.swift
//  MarvelApp
//
//  Created by c94283a on 13/01/22.
//

import UIKit
import CoreData
import Network
import Firebase


class MainViewController: UIViewController, ViewProtocol {
    
    lazy var viewModel : ViewModelProtocol = {
        let viewModel = ViewModel(view: self,
                                  apiRequest: ApiRequest(urlProvider: URLProvider(offsetManage: OffsetManage())),
                                  monitor: NetworkMonitor(),
                                  dataBaseController: DataBaseController(),
                                  urlProvider: URLProvider(offsetManage: OffsetManage()),
                                  analytics: FirebaseAnalytics())
        
        return viewModel
    }()
    
    @IBOutlet weak var carouselCollection: UICollectionView!
    @IBOutlet weak var listCollection: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    
    let monitor = NetworkMonitor()
    var isOnline: Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        monitor.startMonitoring()
        
        setDelegates()
        setDataSourses()
        
        viewModel.view = self
        viewModel.loadContent()
        
        setUpCustimizationsInView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewModel.analytics.screenAnalytics(screenName: "MainScreen", screenClass: "MainViewController" )
        viewModel.analytics.setUserIDForAnalytics()
        viewModel.analytics.setUserPropertyForAnalytics(property: "food", forName: "apple")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        monitor.stopMonitoring()
    }
    
    func reloadContentUp() {
        DispatchQueue.main.async {
            self.carouselCollection.reloadData()
            self.hideAnimation()
        }
    }
    
    func reloadContentDown() {
        DispatchQueue.main.async {
            self.listCollection.reloadData()
            self.hideAnimation()
        }
    }
    
    func showApiErrorAlert(alertTitle:String,alertMenssage: String){
        DispatchQueue.main.async {
            self.hideAnimation()
            let dialogMessage = UIAlertController(title: alertTitle, message: alertMenssage, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                print("Ok button tapped")
            })
            dialogMessage.addAction(ok)
            self.present(dialogMessage, animated: true, completion: nil)
        }
    }
    
    
    func setUpCustimizationsInView(){
        customNavItem()
    }
    
    func setDelegates(){
        searchBar.delegate = self
        carouselCollection.delegate = self
        listCollection.delegate = self
    }
    
    func setDataSourses(){
        carouselCollection.dataSource = self
        listCollection.dataSource = self
    }
    
}



