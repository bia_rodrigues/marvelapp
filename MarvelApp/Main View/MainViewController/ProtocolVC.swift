//
//  Protocols.swift
//  MarvelApp
//
//  Created by c94283a on 14/01/22.
//


protocol ViewProtocol: AnyObject{

    func reloadContentUp()
    func reloadContentDown()
    
    func showAnimation()
    func hideAnimation()
    
    func showApiErrorAlert(alertTitle:String,alertMenssage: String)
}
