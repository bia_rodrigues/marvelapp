//
//  CustomNavBar.swift
//  MarvelApp
//
//  Created by c94283a on 11/01/22.
//

import UIKit

//Navbar

extension MainViewController{
    
    func customNavItem(){
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "Logo"))
        self.navigationItem.accessibilityTraits = .header
        self.navigationItem.accessibilityLabel="MARVEl"
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 32.0/255.0, green: 32.0/255.0, blue: 32.0/255.0, alpha: 1.0)
        
        let backBarBtnItem = UIBarButtonItem()
        backBarBtnItem.title = ""
        backBarBtnItem.tintColor = .white
        navigationItem.backBarButtonItem = backBarBtnItem
        
    }
    
}
