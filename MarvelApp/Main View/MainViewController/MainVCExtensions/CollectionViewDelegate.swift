//
//  UICollectionViewDelegate.swift
//  MarvelApp
//
//  Created by c94283a on 11/01/22.
//

import UIKit
import Firebase

extension MainViewController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detail = DetailViewController()
        var selectItem: HeroInCache
        
        if collectionView == carouselCollection{
            selectItem=viewModel.arrayCarousel[indexPath.row]
        }else{
            selectItem = viewModel.arrayList[indexPath.row]
        }
        
        detail.heroSelected = selectItem
        
        self.navigationController?.pushViewController(detail, animated: true)
    }
}
