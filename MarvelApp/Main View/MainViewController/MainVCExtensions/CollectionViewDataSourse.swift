//
//  UICollectionViewDataSourse.swift
//  MarvelApp
//
//  Created by c94283a on 11/01/22.
//

import UIKit

extension MainViewController:  UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if  collectionView == carouselCollection {
            return viewModel.arrayCarousel.count
        }
        else {
            return viewModel.arrayList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if collectionView == carouselCollection{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "carouselCell", for: indexPath) as? CarouselCell else {return UICollectionViewCell()}
            
            let character = viewModel.arrayCarousel[indexPath.row]
            cell.carouselLabel.text = character.name
            
            if let stringURL = character.image{
                if let url = URL(string: stringURL){
                    if !UIAccessibility.isVoiceOverRunning{
                        cell.carouselImage.kf.setImage(with: url,
                                                       placeholder: UIImage(named: "mask"),
                                                       options: [.transition(.fade(0.7))],
                                                       progressBlock: nil)
                        cell.carouselImage.layer.cornerRadius = 14.0
                    }else{
                        cell.carouselImage.isHidden = true
                        cell.carouselLabel.textColor = .black
                        cell.carouselLabel.accessibilityTraits = .button
                    }
                }
            }
            return cell
        }else{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listCell", for: indexPath) as? ListCell else {return UICollectionViewCell()}
            let character = viewModel.arrayList[indexPath.row]
            cell.listLabel.text = character.name
            
            if let stringURL = character.image{
                if let url = URL(string: stringURL){
                    if !UIAccessibility.isVoiceOverRunning{
                        cell.listImage.kf.setImage(with: url,
                                                   placeholder: UIImage(named: "mask"),
                                                   options: [.transition(.fade(0.7))],
                                                   progressBlock: nil)
                        cell.listImage.layer.cornerRadius = 14.0
                    }else{
                        cell.listImage.isHidden = true
                        cell.listLabel.textColor = .black
                        cell.listLabel.accessibilityTraits = .button
                    }
                    
                }
            }
            return cell
        }
    }
}
