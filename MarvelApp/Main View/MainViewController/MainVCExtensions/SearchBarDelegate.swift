//
//  SearchBarDelegate.swift
//  MarvelApp
//
//  Created by c94283a on 11/01/22.
//

import UIKit


extension MainViewController: UISearchBarDelegate, UIBarPositioningDelegate{
    
    func searchBar(_ searchBar: UISearchBar,textDidChange searchText: String){
        
        viewModel.filteredData=[]
        
        if viewModel.arrayListBackUp.count<viewModel.arrayList.count{
            viewModel.arrayListBackUp = viewModel.arrayList
        }
        
        if searchText != "" {
//            if monitor.isReachable && viewModel.searchResultsFromApiByName.isEmpty {     //FIXME: searchCallToApi  inactive
//                viewModel.searchCallToApi(searchHeroStartingWith: searchText)
////            **sair da função vez que o reload nesse caso tem que ser assincrono ou é valido dar mais de um reload? 
//             }else if  monitor.isReachable{
//            viewModel.arrayList=viewModel.searchResultsFromApiByName.filter{ $0.name!.hasPrefix(searchText)}
//            }else{
            viewModel.filteredData=viewModel.heroInCache.filter{ $0.name!.hasPrefix(searchText)}
            viewModel.arrayList = viewModel.filteredData
            viewModel.apiRequest.isRequestingApi = true
//            }
 //           viewModel.apiRequest.isRequestingApi = true
        }else{
            viewModel.apiRequest.isRequestingApi = false
            viewModel.arrayList = viewModel.arrayListBackUp
            viewModel.searchResultsFromApiByName=[]
        }
        self.listCollection.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.apiRequest.isRequestingApi = false
        viewModel.arrayList = viewModel.arrayListBackUp
        viewModel.searchResultsFromApiByName=[]
        listCollection.reloadData()
        hideAnimation()
    }
    
}
