//
//  InfiniteScroll.swift
//  MarvelApp
//
//  Created by c94283a on 11/01/22.
//

import UIKit

// Infinite Scroll / API Recaller

extension MainViewController : UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height {
                showAnimation()
                viewModel.incrementalCallToApi()
        }
    }
}
