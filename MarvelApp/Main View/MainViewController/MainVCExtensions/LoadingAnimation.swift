//
//  LoadingAnimation.swift
//  MarvelApp
//
//  Created by c94283a on 11/01/22.
//

import UIKit

extension MainViewController{

    
    func hideAnimation(){
        loadIndicator.isHidden = true

    }
    func showAnimation(){
        loadIndicator.isHidden = false

    }
}
