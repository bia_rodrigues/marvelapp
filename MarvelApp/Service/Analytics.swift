//
//  Analytics.swift
//  MarvelApp
//
//  Created by c94283a on 31/01/22.
//

import Foundation
import Firebase

protocol AnalyticsProtocol  {
    func screenAnalytics(screenName: String, screenClass: String )
    func setUserPropertyForAnalytics(property: String, forName: String)
    func setLogEventForAnalytics(key:String,heroName: String)
    func setUserIDForAnalytics()
}

struct FirebaseAnalytics: AnalyticsProtocol {

    func screenAnalytics(screenName: String, screenClass: String ){
       Analytics.logEvent(AnalyticsEventScreenView,
                   parameters: [AnalyticsParameterScreenName: screenName,
                                AnalyticsParameterScreenClass: screenClass])
    }

    func setUserPropertyForAnalytics(property: String, forName: String){
        Analytics.setUserProperty(property, forName: forName)
    }

    func setLogEventForAnalytics(key:String,heroName: String){
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            key:heroName
        ])
    }
    
    func setUserIDForAnalytics() {
       let id : String = Bundle.main.object(forInfoDictionaryKey:  "privateKey") as! String
         Analytics.setUserID(id)
     }

}
