//
//  ErrorCases.swift
//  MarvelApp
//
//  Created by c94283a on 09/12/21.
//

import Foundation

enum ApiError: Error {

    case urlInvalid
    case apiInvalid
    case noHero

}
