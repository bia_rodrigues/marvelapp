//
//  ApiRequest.swift
//  MarvelApp
//
//  Created by c94283a on 26/11/21.
//


import Foundation
import CoreData

protocol RequestProtocol{
    
    var urlProvider: URLProviderProtocol { get set }
    
    var isRequestingApi: Bool { get set }
    var url: String { get set }
    var shouldIncreaseOffset: Bool { get set }
    
    func makeRequest(completion: @escaping(Result<Welcome, Error>) -> ())
    
}

class ApiRequest: RequestProtocol{
    
    var urlProvider: URLProviderProtocol
    var offsetManage = OffsetManage()
    var isRequestingApi : Bool = false
    var shouldIncreaseOffset: Bool = true
    var url: String = ""
    
    init(urlProvider: URLProviderProtocol) {
        self.urlProvider = urlProvider
    }
    
    func makeRequest(completion: @escaping(Result<Welcome, Error>) -> ()){
        if isRequestingApi == false{
            let url = url
            if let url = URL(string: url) {
                isRequestingApi = true
                let task = URLSession.shared.dataTask(with: url){(data, response, error) in
                    self.isRequestingApi = false
                    do{
                        guard let responseData = data else { return }//call compleition and error  alert
                        let posts = try JSONDecoder().decode(Welcome.self, from: responseData)
                        if self.shouldIncreaseOffset{
                            self.offsetManage.offsetKepper(posts: posts)
                        }
                        completion(.success(posts))
                    }catch{
                        completion(.failure(ApiError.urlInvalid))
                    }
                }
                task.resume()
            }
        }
    }
    
}
