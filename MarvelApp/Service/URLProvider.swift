//
//  URLProvider.swift
//  MarvelApp
//
//  Created by c94283a on 13/01/22.
//

import Foundation
import CommonCrypto

protocol URLProviderProtocol{
    mutating func getURL()->String
    func getSearchURL(for searchParameter:String) -> String
    
    var offsetManage : ProtocolOffsetManage { get set }
}

struct URLProvider: URLProviderProtocol{
    
    var offsetManage: ProtocolOffsetManage
    
    init(offsetManage: ProtocolOffsetManage) {
        self.offsetManage = offsetManage
    }
    
    
    let baseURL : String = "http://gateway.marvel.com"
    let path: String = "v1/public/characters"
    let searchByFirstLetterPath:String = "v1/public/characters?nameStartsWith="
    let publicKey = Bundle.main.object(forInfoDictionaryKey: "publicKey")as! String
    let privateKey = Bundle.main.object(forInfoDictionaryKey: "privateKey")as! String
    var isRequestingApi : Bool = false

    mutating func getURL() -> String {
        let ts = Int(Date().timeIntervalSince1970)
        let content = String(ts) + privateKey + publicKey
        let hash = MD5(string: content)
        // URL
        let offset =  offsetManage.fetchOffset()
        let url = baseURL + "/" + path + "?" + "ts=\(ts)" + "&apikey=\(publicKey)" + "&hash=\(hash)"+"&offset=\(offset)"
        print(url)
        return url
    }
    
    func getSearchURL(for searchParameter:String) -> String {
        let ts = Int(Date().timeIntervalSince1970)
        let content = String(ts) + privateKey + publicKey
        let hash = MD5(string: content)
        // URL
        let offset = 0
        print("parametro",searchParameter)
        let url = "\(baseURL)/\(searchByFirstLetterPath)\(searchParameter)&offset=\(offset)&apikey=\(publicKey)&hash=\(hash)&ts=\(ts)"
        print(url)
        return url
    }
    
    
    func MD5(string: String) -> String {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: length)
        
        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData.map { String(format: "%02hhx", $0) }.joined()
    }
    
}
