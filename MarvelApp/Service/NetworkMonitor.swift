//
//  NetworkMonitor.swift
//  MarvelApp
//
//  Created by c94283a on 11/01/22.
//

import Foundation
import Network

protocol NetworkMonitorProtocol{
    var isReachable: Bool { get }
    func startMonitoring()
    func stopMonitoring()
}

class NetworkMonitor: NetworkMonitorProtocol {
    static let shared = NetworkMonitor()

    let monitor = NWPathMonitor()
    private var status: NWPath.Status = .requiresConnection
    var isReachable: Bool { status == .satisfied }
    var isReachableOnCellular: Bool = true

    func startMonitoring() {
        monitor.pathUpdateHandler = { [weak self] path in
            self?.status = path.status
            self?.isReachableOnCellular = path.isExpensive

            if path.status == .satisfied {
                print("We're connected!")
                // post connected notification
            } else {
                print("No connection.")
              //  self.alert?.alert(message: "No connection.", title: "Ops")
                // post disconnected notification
            }
            print(path.isExpensive)
        }

        let queue = DispatchQueue(label: "NetworkMonitor")
        monitor.start(queue: queue)
    }

    func stopMonitoring() {
        monitor.cancel()
    }
}
