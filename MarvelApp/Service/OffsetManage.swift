//
//  File.swift
//  MarvelApp
//
//  Created by c94283a on 09/01/22.
//

import Foundation

protocol ProtocolOffsetManage{
    func apiOffsetSaver(offset:Int16)
    mutating func fetchOffset() -> Int16
}

struct OffsetManage: ProtocolOffsetManage{
    
    let dataBaseController = DataBaseController()
    let countOfHerosInEachCompletePage : Int = 20
    
    
    /// set offset in coredata
    func apiOffsetSaver(offset:Int16) {//
        let context = DataBaseController.persistentContainer.viewContext
        let newOffset = Offset(context: context)
        newOffset.nextFirstReturnFromAPI = offset
        
        dataBaseController.saveContext()
    }
    
    /// get offset from coredata
    mutating func fetchOffset() -> Int16 {//
        do{
            let data = try DataBaseController.persistentContainer.viewContext.fetch(Offset.fetchRequest())
            let index = data.count
            if index > 0 {
                return data[index-1].nextFirstReturnFromAPI
            }
            return 0
        } catch {
            print("Erro in fetchData")
            return 0
        }
        
    }
    
    func getOffsetCount()-> Int16{
        do{
            let dataFromCache = try DataBaseController.persistentContainer.viewContext.fetch(HeroInCache.fetchRequest())
            return Int16(dataFromCache.count)
        }
        catch{
            print("Erro in fetchData")
            return Int16(0)
        }
    }
    
    /// decides if offset in coredata needs to be setted
    mutating func offsetKepper(posts : Welcome){
        if posts.data.listaDeHerois.count == self.countOfHerosInEachCompletePage{
            let lastFirstReturnFromAPI: Int = Int(fetchOffset())
            let nextFirstReturnFromAPI = lastFirstReturnFromAPI + posts.data.listaDeHerois.count
            self.apiOffsetSaver(offset: Int16(nextFirstReturnFromAPI))
        }else{
            return
        }
    }
    
}


